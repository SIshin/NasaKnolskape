package com.knolskape.knolskapetask;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.HandlerThread;


/**
 * Created by SISHIN on 10/15/16.
 */
public class App extends Application {

    public final Handler mUIHandler = new Handler();
    private static final HandlerThread sWorkerThread = new HandlerThread("app_worker");


    static {
        sWorkerThread.start();
    }

    private static final Handler sWorkerHandler = new Handler(sWorkerThread.getLooper());


    @Override
    public void onCreate() {
        super.onCreate();
    }


    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }


    public Handler getWorkHandler() {
        return sWorkerHandler;
    }


    public Handler getUIHandler() {
        return mUIHandler;
    }
}
