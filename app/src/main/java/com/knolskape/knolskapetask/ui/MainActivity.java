package com.knolskape.knolskapetask.ui;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.knolskape.knolskapetask.R;
import com.knolskape.knolskapetask.interfaces.IResultListener;
import com.knolskape.knolskapetask.loader.DetailsLoader;
import com.knolskape.knolskapetask.provider.PreferenceGeneral;
import com.knolskape.knolskapetask.provider.Tables;
import com.knolskape.knolskapetask.server.UIController;
import com.knolskape.knolskapetask.server.response.BasicResponse;
import com.knolskape.knolskapetask.utils.Constants;
import com.knolskape.knolskapetask.utils.L;

public class MainActivity extends AppCompatActivity {

    private final DetailsCallBacks mDetailsCallback = new DetailsCallBacks();
    private Context mContext;

    private ImageView mImage;
    private TextView mDescription;
    ProgressDialog mProgress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_home);

        mImage = (ImageView) findViewById(R.id.image);
        mDescription = (TextView) findViewById(R.id.textDescription);
        mProgress = new ProgressDialog(mContext);

        getSupportLoaderManager().initLoader(mDetailsCallback.hashCode(), null, mDetailsCallback);

        getDetailsCall();
    }


    public class DetailsCallBacks implements LoaderManager.LoaderCallbacks<DetailsLoader.Result> {


        @Override
        public Loader<DetailsLoader.Result> onCreateLoader(int id, Bundle args) {

            return new DetailsLoader(mContext);
        }

        @Override
        public void onLoadFinished(Loader<DetailsLoader.Result> loader, DetailsLoader.Result data) {


            if (data.cursor != null && data.cursor.moveToFirst()) {

                mDescription.setText(data.cursor.getString(data.cursor.getColumnIndexOrThrow(Tables.Nasa.DESCRIPTION)));
                Glide.with(mContext)
                        .load(data.cursor.getString(data.cursor.getColumnIndexOrThrow(Tables.Nasa.MEDIA_URL)))
                        .placeholder(R.drawable.place_holder) // can also be a drawable
                        .crossFade()
                        .into(mImage);

            }

        }

        @Override
        public void onLoaderReset(Loader<DetailsLoader.Result> loader) {

        }

    }

    private void getDetailsCall() {

        long ts = PreferenceGeneral.getInstance(mContext).getLong(PreferenceGeneral.GET_DETAILS_TS, -1);
        if (ts == -1 || System.currentTimeMillis() - ts > Constants.TIME.DAY) {
            mProgress.setMessage("Fetching data...) ");
            mProgress.show();
            UIController.getDetails(this, new BasicResponse(), new IResultListener<BasicResponse>() {
                @Override
                public void onResult(BasicResponse result) {
                    if (mProgress != null && mProgress.isShowing()) {
                        mProgress.dismiss();
                    }
                    if (result.success) {
                        PreferenceGeneral.getInstance(mContext).addPreference(PreferenceGeneral.GET_DETAILS_TS, System.currentTimeMillis());
                    }
                }
            });


        }

    }
}



