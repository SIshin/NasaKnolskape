package com.knolskape.knolskapetask.provider;


import android.content.ContentValues;
import android.content.Context;

import com.knolskape.knolskapetask.server.response.BasicResponse;
import com.knolskape.knolskapetask.utils.Constants;
import com.knolskape.knolskapetask.utils.L;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by SISHIN on 10/15/16.
 */

public class DbHelper {


    public static void writeDetails(Context context, BasicResponse response) throws JSONException {

        L.d("write started");


        try {
            context.getContentResolver().delete(Tables.Nasa.CONTENT_URI_NO_NOTIFICATION, null, null);

            JSONObject jsonObject = new JSONObject(response.responseContent);
            ContentValues cv = new ContentValues();
            cv.put(Tables.Nasa.TITLE, jsonObject.getString(Constants.JsonConstants.TITLE));
            cv.put(Tables.Nasa.DESCRIPTION, jsonObject.getString(Constants.JsonConstants.EXPLANATION));
            cv.put(Tables.Nasa.MEDIA_URL, jsonObject.getString(Constants.JsonConstants.URL));
            cv.put(Tables.Nasa.MEDIA_TYPE, jsonObject.getString(Constants.JsonConstants.MEDIA_TYPE));
            cv.put(Tables.Nasa.DATE, jsonObject.getString(Constants.JsonConstants.DATE));

            context.getContentResolver().insert(Tables.Nasa.CONTENT_URI, cv);
            L.d("write DONE");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
