package com.knolskape.knolskapetask.provider;

import android.net.Uri;

import com.knolskape.knolskapetask.utils.Constants;


/**
 * Created by SISHIN on 10/15/16.
 */
public class Tables {

    public static Uri.Builder appendCustom(Uri.Builder builder) {
        return builder.appendQueryParameter(Constants.Provider.PARAMETER_CUSTOMQUERY, "true");
    }


    public static final class Registry {
        public static final String TABLENAME = "registry";
        public static final String KEY = "key";
        public static final String VALUE = "value";

        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + Constants.Provider.AUTHORITY + "/" +
                TABLENAME +
                "?" +
                Constants.Provider.PARAMETER_NOTIFY +
                "=true");

        /**
         * The content:// style URL for this table. When this Uri is used, no notification is
         * sent if the content changes.
         */
        public static final Uri CONTENT_URI_NO_NOTIFICATION = Uri.parse("content://" + Constants.Provider.AUTHORITY +
                "/" +
                TABLENAME +
                "?" +
                Constants.Provider
                        .PARAMETER_NOTIFY +
                "=false");

    }

    public static final class Nasa {
        public static final String TABLENAME = "Nasa";

        public static final String TITLE = "title";
        public static final String MEDIA_URL = "media_url";
        public static final String MEDIA_TYPE = "media_type";
        public static final String DESCRIPTION = "description";
        public static final String DATE = "date";

        /**
         * The content:// style URL for this table
         */
        public static final Uri CONTENT_URI = Uri.parse("content://" + Constants.Provider.AUTHORITY + "/" +
                TABLENAME +
                "?" +
                Constants.Provider.PARAMETER_NOTIFY +
                "=true");

        /**
         * The content:// style URL for this table. When this Uri is used, no notification is
         * sent if the content changes.
         */
        public static final Uri CONTENT_URI_NO_NOTIFICATION = Uri.parse("content://" + Constants.Provider.AUTHORITY +
                "/" +
                TABLENAME +
                "?" +
                Constants.Provider
                        .PARAMETER_NOTIFY +
                "=false");
    }

    public static final String CREATE_REGISTRY_TABLE =
            " CREATE TABLE " + Registry.TABLENAME
                    + " ( "
                    + Registry.KEY
                    + " TEXT  PRIMARY KEY , "
                    + Registry.VALUE
                    + " TEXT );";
    public static final String CREATE_NASA_TABLE =
            " CREATE TABLE " + Nasa.TABLENAME
                    + " ( "
                    + Nasa.TITLE
                    + " TEXT , "
                    + Nasa.MEDIA_URL
                    + " TEXT , "
                    + Nasa.MEDIA_TYPE
                    + " TEXT , "
                    + Nasa.DESCRIPTION
                    + " TEXT , "
                    + Nasa.DATE
                    + " TEXT)";

}
