package com.knolskape.knolskapetask.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.text.TextUtils;

import com.knolskape.knolskapetask.utils.Constants;
import com.knolskape.knolskapetask.utils.L;


/**
 * Created by SISHIN on 10/15/16.
 */
public class Provider extends ContentProvider {

    private DatabaseHelper mOpenHelper;
    public static final String TAG = "Provider";


    @Override
    public boolean onCreate() {
        final Context context = getContext();
        mOpenHelper = new DatabaseHelper(context);
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(args.table);
        SQLiteDatabase db = mOpenHelper.getReadableDatabase();
        Cursor result;
        if (args.iscustomquery) {
            result = db.rawQuery(args.query, null);
        } else {
            result = qb.query(db, projection, args.where, args.args, null, null, sortOrder);
        }
        result.setNotificationUri(getContext().getContentResolver(), uri);

        return result;
    }

    @Override
    public String getType(Uri uri) {
        SqlArguments args = new SqlArguments(uri, null, null);
        if (TextUtils.isEmpty(args.where)) {
            return "vnd.android.cursor.dir/" + args.table;
        } else {
            return "vnd.android.cursor.item/" + args.table;
        }
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        SqlArguments args = new SqlArguments(uri);

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        final long rowId = dbInsertAndCheck(mOpenHelper, db, args.table, null, values);
        if (rowId <= 0) return null;

        uri = ContentUris.withAppendedId(uri, rowId);
        sendNotify(uri);

        return uri;
    }

    private static long dbInsertAndCheck(DatabaseHelper helper, SQLiteDatabase db, String table,
                                         String nullColumnHack, ContentValues values) {
        if (values == null) {
            throw new RuntimeException("Error: attempting to insert null values");
        }
        return db.insertWithOnConflict(table, nullColumnHack, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        SqlArguments args = new SqlArguments(uri);

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            int numValues = values.length;
            for (int i = 0; i < numValues; i++) {
                if (dbInsertAndCheck(mOpenHelper, db, args.table, null, values[i]) < 0) {
                    return 0;
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }

        sendNotify(uri);
        return values.length;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);

        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = db.delete(args.table, args.where, args.args);
        if (count > 0) sendNotify(uri);

        return count;

    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SqlArguments args = new SqlArguments(uri, selection, selectionArgs);
        SQLiteDatabase db = mOpenHelper.getWritableDatabase();
        int count = 0;
        if (args.iscustomquery) {
            db.execSQL(args.getUpdateQuery(values), selectionArgs);
        } else {
            count = db.update(args.table, values, args.where, args.args);
        }
        if (count > 0) sendNotify(uri);

        return count;
    }


    private void sendNotify(Uri uri) {
        String notify = uri.getQueryParameter(Constants.Provider.PARAMETER_NOTIFY);
        if (notify == null || "true".equals(notify)) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, Constants.Provider.DATABASE_NAME, null, Constants.Provider.DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(Tables.CREATE_REGISTRY_TABLE);
            db.execSQL(Tables.CREATE_NASA_TABLE);

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            L.d(TAG, "onUpgrade triggered: " + oldVersion);
        }
    }

    static class SqlArguments {
        public final String table;
        public final String where;
        public final String[] args;
        public boolean iscustomquery = false;
        public String query;

        SqlArguments(Uri uri, String where, String[] args) {
            String customParam = uri.getQueryParameter(Constants.Provider.PARAMETER_CUSTOMQUERY);
            if (customParam != null && customParam.trim().equals("true")) {
                this.iscustomquery = true;
                query = where;
            }

            if (uri.getPathSegments().size() == 1) {
                this.table = uri.getPathSegments().get(0);
                this.where = where;
                this.args = args;
            } else if (uri.getPathSegments().size() != 2) {
                if (!this.iscustomquery)
                    throw new IllegalArgumentException("Invalid URI: " + uri);
                else {
                    this.table = "";
                    this.where = where;
                    this.args = args;
                }
            } else {
                this.table = uri.getPathSegments().get(0);
                this.where = "_id=" + ContentUris.parseId(uri);
                this.args = null;
            }

        }

        public String getUpdateQuery(ContentValues values) {
            StringBuilder sql = new StringBuilder(120);
            sql.append("UPDATE ");
            sql.append(table);
            sql.append(" SET ");
            // move all bind args to one array
            int i = 0;
            for (String colName : values.keySet()) {
                sql.append((i > 0) ? "," : "");
                sql.append(colName);
                sql.append("=");
                sql.append(values.get(colName));
                i++;
            }
            if (!TextUtils.isEmpty(where)) {
                sql.append(" WHERE ");
                sql.append(where);
            }
            return sql.toString();
        }


        SqlArguments(Uri url) {
            if (url.getPathSegments().size() == 1) {
                table = url.getPathSegments().get(0);
                where = null;
                args = null;
            } else {
                throw new IllegalArgumentException("Invalid URI: " + url);
            }
        }
    }
}
