package com.knolskape.knolskapetask.utils;



/**
 * Created by SISHIN on 10/15/16.
 */
public class Constants {

    public static final boolean isLogEnabled = true;
    public static final boolean isProduction = false;



    public class Provider {
        public static final String DATABASE_NAME = "TF.db";
        public static final int DATABASE_VERSION = 2;
        public static final String PARAMETER_CUSTOMQUERY = "customquery";
        public static final String AUTHORITY = "com.knolskape.provider";
        public static final String SQL_DESC = " DESC";
        public static final String SQL_ASC = " ASC";
        public static final String PARAMETER_NOTIFY = "notify";
        public static final String SQL_LIMIT = " LIMIT 1";
    }

    public class JsonConstants {
        public static final String DATA = "data";

        public static final String MESSAGE = "message";

        public static final String ERROR = "error";
        public static final String CODE = "code";
        public static final String MEDIA_TYPE = "media_type";
        public static final String EXPLANATION = "explanation";
        public static final String URL = "url";
        public static final String TITLE = "title";
        public static final String DATE = "date";


    }
    public static class TIME {
        public static final long HOUR = 3600000;
        public static final long DAY = 24 * HOUR;
        public static final long WEEK = 7 * DAY;
    }


}
