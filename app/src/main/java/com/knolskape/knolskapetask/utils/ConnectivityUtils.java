package com.knolskape.knolskapetask.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectivityUtils {

    /**
     * return true if device is connected to any data network.
     *
     * @param context
     * @return true if connected to data network, false otherwise
     */
    public static boolean isNetworkAvailable(Context context) {
        if (context == null)
            return false;
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()
                && netInfo.isAvailable() && netInfo.isConnected()) {
            return true;
        }
        return false;
    }
}