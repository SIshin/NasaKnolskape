package com.knolskape.knolskapetask.loader;


import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;

import com.knolskape.knolskapetask.provider.Tables;
import com.knolskape.knolskapetask.utils.L;

/**
 * Created by SISHIN on 10/16/16.
 */

public class DetailsLoader extends AsyncLoader<DetailsLoader.Result>{

    public DetailsLoader(Context context) {
        super(context);
    }

    @Override
    protected void registerContentObserver(ContentObserver observer) {
        mContext.getContentResolver().registerContentObserver(Tables.Nasa.CONTENT_URI, true, observer);
    }

    @Override
    protected void unRegisterContentObserver(ContentObserver observer) {
        mContext.getContentResolver().unregisterContentObserver(observer);

    }

    @Override
    public DetailsLoader.Result loadInBackground() {
        DetailsLoader.Result result = new DetailsLoader.Result();
        Cursor cursor = mContext.getContentResolver().query(Tables.Nasa.CONTENT_URI, null, null, null, null);
        result.cursor = cursor;

        L.d(cursor.getCount()+"::::::::::::::::::::::");
        return result;
    }

    public class Result extends BaseResult {
        public Cursor cursor;
    }
}
