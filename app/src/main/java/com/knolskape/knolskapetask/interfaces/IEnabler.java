package com.knolskape.knolskapetask.interfaces;

public interface IEnabler {
    public void setEnabled(boolean enable);
}
