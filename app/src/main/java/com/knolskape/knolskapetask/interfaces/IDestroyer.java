package com.knolskape.knolskapetask.interfaces;

public interface IDestroyer {
    public void destroy();
}
