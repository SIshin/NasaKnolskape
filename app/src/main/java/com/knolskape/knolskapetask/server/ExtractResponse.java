package com.knolskape.knolskapetask.server;

import android.content.Context;

import com.knolskape.knolskapetask.server.response.BasicResponse;
import com.knolskape.knolskapetask.utils.Constants;

import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by SISHIN on 10/15/16.
 */
public class ExtractResponse {

    protected static void extractResponse(Context context, BasicResponse response) throws JSONException {
        if (response.responseCode == BasicResponse.OKAY)
            if (response.responseContent != null) {
                JSONObject content = new JSONObject(response.responseContent);
                if (content.has(Constants.JsonConstants.ERROR)) {
                    response.success = false;
                    response.customErrorCode = content.getInt(Constants.JsonConstants.CODE);
                    response.customErrorMessage = content.getString(Constants.JsonConstants.MESSAGE);
                } else {
                    response.success = true;
                }
            } else {
                response.success = false;
            }
    }


}
