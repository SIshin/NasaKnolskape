package com.knolskape.knolskapetask.server;

import android.content.Context;
import android.database.sqlite.SQLiteDiskIOException;

import com.knolskape.knolskapetask.provider.DbHelper;
import com.knolskape.knolskapetask.server.response.BasicResponse;
import org.json.JSONException;
import java.io.IOException;


/**
 * Created by SISHIN on 10/15/16.
 */
public class Api {





    public static void getDetails(Context context, BasicResponse response, int retryCount) {

        try {
            ApiFactory.getDetails(context, response);
            if (response.success && response.responseContent != null) {
               DbHelper.writeDetails(context, response);
            }
        } catch (Exception e) {
            extractException(context, response, e);
            e.printStackTrace();
        }
    }




    /**
     * this function will handle all the exceptions related to http calls to server. the exception
     * message will be extracted and exception type will be saved in response class.
     *
     * @param context
     * @param response {@link BasicResponse} response object saves the
     *                 exception details
     * @param e        exception to extract for the details.
     */
    private static void extractException(final Context context, BasicResponse response, Exception e) {
        if (e instanceof SQLiteDiskIOException) {
            response.responseCode = BasicResponse.IO_EXE;
            response.success = false;
        } else if (e instanceof IOException) {
            response.responseCode = BasicResponse.IO_EXE;
            response.success = false;
        } else if (e instanceof JSONException) {
            response.responseCode = BasicResponse.JSON_EXE;
            response.success = false;
        }
    }
}
