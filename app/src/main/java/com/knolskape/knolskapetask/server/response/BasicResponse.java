package com.knolskape.knolskapetask.server.response;

import android.content.Context;

import java.io.Serializable;


/**
 * Created by SISHIN on 10/15/16.
 */
public class BasicResponse implements Serializable {

    public final static int OKAY = 200;
    public final static int CREATED = 201;
    public final static int ACCEPTED = 202;
    public final static int BAD_REQUEST = 400;
    public final static int FORBIDDEN = 403;
    public final static int NOT_FOUND = 404;
    public final static int REQUEST_TIMEOUT = 408;
    public final static int DUPLICATE_ENTRY = 409;
    public final static int TOKEN_EXPIRED = 412;
    public final static int INTERNAL_SERVER_ERROR = 500;
    public final static int THROTTLED = 503;


    public final static int NO_RESULTS = 1004;
    public final static int IO_EXE = 1001; //response code for SQLiteDiskIOException, IOException
    public final static int JSON_EXE = 1003;

    public String mToken="8P2yIhNNudEKmbTqTNsFK4XPsFD34NU3jgzIOI75";
    public boolean success;
    public int responseCode;
    public String responseContent;
    public String customErrorMessage="Oops, Something went wrong" ;
    public int customErrorCode;



    public String getCustomErrorMessage(Context context) {
//        switch (responseCode) {
//            case OKAY: {
//                return customErrorMessage;
//            }
//            case BAD_REQUEST: {
//                return context.getString(R.string.bad_request);
//            }
//            case FORBIDDEN: {
//                return context.getString(R.string.forbidden);
//            }
//            case NOT_FOUND: {
//                return context.getString(R.string.not_implemented);
//            }
//            case REQUEST_TIMEOUT: {
//                return context.getString(R.string.io_error);
//            }
//            case DUPLICATE_ENTRY: {
//                return context.getString(R.string.duplicate_entry);
//            }
//            case INTERNAL_SERVER_ERROR: {
//                return context.getString(R.string.default_error);
//            }
//            case THROTTLED: {
//                return context.getString(R.string.throttled);
//            }
//            case IO_EXE: {
//                return context.getString(R.string.please_check_internet_settings);
//            }
//        }
        return "Unknown error";
    }
}
