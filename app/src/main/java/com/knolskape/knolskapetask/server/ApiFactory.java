package com.knolskape.knolskapetask.server;

import android.content.Context;
import android.net.Uri;

import com.knolskape.knolskapetask.server.response.BasicResponse;
import com.knolskape.knolskapetask.utils.ConnectivityUtils;
import com.knolskape.knolskapetask.utils.Constants;
import com.knolskape.knolskapetask.utils.L;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.Response;

import org.json.JSONException;

import java.io.IOException;



/**
 * Created by SISHIN on 10/15/16.
 */
public class ApiFactory extends ExtractResponse {

    static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    protected static final String DEVICE_TYPE = "device_type";



    private static final int GET_DETAILS = 0;

    private static final String HOST_NAME_DEV = "api.nasa.gov";

    private static final String HOST_NAME_PROD = "api.nasa.gov";
    public static final String HOST_NAME = Constants.isProduction ? HOST_NAME_PROD : HOST_NAME_DEV;


    private static String getUrl(Context context, UrlBuilder urlBuilder) {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("https");
        builder.authority(HOST_NAME);
        switch (urlBuilder.mUrlType) {

            case GET_DETAILS: {
                builder.appendPath("planetary/apod");
                if (urlBuilder.mResponse.mToken != null)
                    builder.appendQueryParameter("api_key", urlBuilder.mResponse.mToken + "");

                break;
            }
        }
        return builder.build().toString();
    }


    protected static void getDetails(Context context, BasicResponse response) throws IOException, JSONException {
        if (response.mToken == null) {
            response.responseCode = BasicResponse.FORBIDDEN;
            response.success = false;
            return;
        }
        if (ConnectivityUtils.isNetworkAvailable(context)) {
            UrlBuilder urlBuilder = new UrlBuilder();
            urlBuilder.mUrlType = GET_DETAILS;
            urlBuilder.mResponse = response;
            ParamsBuilder paramsBuilder = new ParamsBuilder().build(response);
            paramsBuilder.mContext = context;
            paramsBuilder.mApiType = GET_DETAILS;


            Response httpResponse = ServerRequest.doGetRequest(context, getUrl(context, urlBuilder), null);
            response.responseCode = httpResponse.code();
            response.responseContent = httpResponse.body().string();
            L.d("claimed shops " + "Code :" + response.responseCode + " content", response.responseContent.toString());
            extractResponse(context, response);
        } else {
            response.success = false;
            response.responseCode = BasicResponse.IO_EXE;
        }
    }


}
