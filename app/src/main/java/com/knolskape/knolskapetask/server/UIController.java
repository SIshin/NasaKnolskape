package com.knolskape.knolskapetask.server;

import android.content.Context;
import android.os.Handler;

import com.knolskape.knolskapetask.App;
import com.knolskape.knolskapetask.interfaces.IResultListener;
import com.knolskape.knolskapetask.server.response.BasicResponse;


/**
 * Created by SISHIN on 10/15/16.
 */
public class UIController {
    public static void getDetails(final Context context, final BasicResponse response, final IResultListener<BasicResponse> listener) {
        Runnable runnable = new Runnable() {
            public void run() {
                Handler handler = ((App) context.getApplicationContext()).getUIHandler();
                Api.getDetails(context, response, 0);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (!listener.equals(null)) listener.onResult(response);
                    }
                });
            }
        };
        new Thread(runnable).start();
    }

}
